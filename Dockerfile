### Prepare the dependencies
FROM ambs/debian-mysql8-perl-alt:latest-build AS builder

RUN apt install -y libmysqlclient-dev libpq-dev

COPY cpanfile* /stack/
RUN  cd /stack && pdi-build-deps --stack

COPY bin /stack/bin/
RUN set -e && cd /stack && for script in bin/* ; do perl -wc $script ; done


### Runtime image
FROM ambs/debian-mysql8-perl-alt:latest-runtime

RUN apt install -y openssh-client

COPY --from=builder /stack /stack

ENTRYPOINT [ "/stack/bin/minion-entrypoint" ]
