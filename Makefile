all:
	@echo "build"
	@echo "push"

build:
	docker build -t ambs/debian-minion .

push: build
	docker push ambs/debian-minion:latest


